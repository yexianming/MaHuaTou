package com.sleep.uulib.widget;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import com.sleep.uulib.R;
import com.sleep.uulib.util.AssetsUtil;
import com.sleep.uulib.util.NumberFormatUtils;
import com.sleep.uulib.util.NumberUtils;


/**
 * Created by LLL on 2017/10/10.
 */

public class CircleProgressView extends View {

    private static final String TAG = "CircleProgressBar";

    private double mMaxProgress = 100;

    private double mProgress = 30;

    private float mCircleLineStrokeWidth = 15;

    private final int mTxtStrokeWidth = 2;

    private String progressText = null;

    //进度背景颜色
    private int mBackgroundColor;

    //进度前景颜色
    private int mForegroundColor;

    // 画圆所在的距形区域
    private final RectF mRectF;

    private final Paint mPaint;

    private final Context mContext;

    private final Typeface textTypeFace;

    public CircleProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;
        mRectF = new RectF();
        mPaint = new Paint();

        AssetManager assetManager = AssetsUtil.getAssetManager(context);
        //根据路径得到Typeface
        textTypeFace = Typeface.createFromAsset(assetManager, "fonts/AlternateGothic2BT.ttf");

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircleProgress);
        mCircleLineStrokeWidth = typedArray.getDimension(R.styleable.CircleProgress_circle_stroke_width,15);
        mBackgroundColor = typedArray.getColor(R.styleable.CircleProgress_circle_backgroundColor,Color.rgb(0xe9, 0xe9, 0xe9));
        mForegroundColor = typedArray.getColor(R.styleable.CircleProgress_circle_foregroundColor,Color.parseColor("#21a3e9"));

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = this.getWidth();
        int height = this.getHeight();

        if (width != height) {
            int min = Math.min(width, height);
            width = min;
            height = min;
        }

        // 设置画笔相关属性
        mPaint.setAntiAlias(true);
        mPaint.setColor(mBackgroundColor);
        canvas.drawColor(Color.TRANSPARENT);
        mPaint.setStrokeWidth(mCircleLineStrokeWidth);
        mPaint.setStyle(Paint.Style.STROKE);
        // 位置
        mRectF.left = mCircleLineStrokeWidth / 2; // 左上角x
        mRectF.top = mCircleLineStrokeWidth / 2; // 左上角y
        mRectF.right = width - mCircleLineStrokeWidth / 2; // 左下角x
        mRectF.bottom = height - mCircleLineStrokeWidth / 2; // 右下角y

        // 绘制圆圈，进度条背景
        canvas.drawArc(mRectF, -90, 360, false, mPaint);
        mPaint.setColor(mForegroundColor);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        float sweepAngle = (float) ((NumberUtils.div(mProgress, mMaxProgress)) * 360);
        canvas.drawArc(mRectF, -90, sweepAngle, false, mPaint);

        // 绘制进度文案显示
        mPaint.setStrokeWidth(mTxtStrokeWidth);
        int textHeight = height / 4;
        mPaint.setTextSize(textHeight);

        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setTypeface(textTypeFace);
        canvas.drawText(getProgressText(), width / 2 , height / 2 + textHeight / 2 -5, mPaint);
    }

    private String getProgressText(){
        if(progressText == null || "".equals(progressText)){
            progressText = NumberFormatUtils.getNumberWithDigital(NumberUtils.mul(NumberUtils.div(mProgress, mMaxProgress), 100.00), 1);
        }
        return progressText;
    }

    public void setProgressText(String progressText){
        this.progressText = progressText;
        this.invalidate();
    }

    public double getMaxProgress() {
        return mMaxProgress;
    }

    public void setMaxProgress(double maxProgress) {
        this.mMaxProgress = maxProgress;
    }

    public void setProgress(double progress) {
        this.mProgress = progress;
        this.invalidate();
    }

    public void setProgressNotInUiThread(int progress) {
        this.mProgress = progress;
        this.postInvalidate();
    }
}
