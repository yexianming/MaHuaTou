package com.fuyou.mahuatou.wxapi;

import android.os.Bundle;

import com.umeng.weixin.callback.WXCallbackActivity;
import com.fuyou.mahuatou.R;

public class WXEntryActivity extends WXCallbackActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wxentry);
    }
}
