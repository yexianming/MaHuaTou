package com.fuyou.mine.fragment


import android.Manifest
import android.content.Intent
import android.net.Uri
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.view.View
import android.widget.TextView
import com.alibaba.android.arouter.facade.annotation.Route
import com.alibaba.android.arouter.launcher.ARouter
import com.example.twitterlib.wigdet.dialog.CommonFragmentDialog
import com.example.twitterlib.wigdet.dialog.DialogFactory
import com.fuyou.mine.R
import com.fuyou.mine.mvp.presenter.MinePresenter
import com.fuyou.mine.mvp.view.IMineView
import com.sleep.commonlib.util.LibCommonUtil
import com.sleep.commonlib.util.LogUtil
import com.sleep.commonlib.util.SPUtil
import com.sleep.commonlib.util.ToastUtil
import com.sleep.uulib.UUApplication
import com.sleep.uulib.account.LoginActivity
import com.sleep.uulib.bean.QueryBankBean
import com.sleep.uulib.bean.QueryUserInfoBean
import com.sleep.uulib.bean.RedBagAndCouponBean
import com.sleep.uulib.constant.ArouterConstant
import com.sleep.uulib.constant.Constant
import com.sleep.uulib.constant.NetConstant
import com.sleep.uulib.mvp.model.CommonModule
import com.sleep.uulib.mvp.view.QueryUserInfoView
import com.sleep.uulib.netWork.NetCommonMethod
import com.sleep.uulib.util.NumberFormatUtils
import com.sleep.uulib.uubase.HtmlUrlActivity
import com.sleep.uulib.uubase.UUBaseFragment
import com.umeng.analytics.MobclickAgent
import com.yanzhenjie.permission.AndPermission
import com.yanzhenjie.permission.PermissionListener
import kotlinx.android.synthetic.main.fragment_mine.*


/**
 * A simple [Fragment] subclass.
 */
@Route(path = ArouterConstant.APP_MINE_FRAGMENT)
class MineFragment : UUBaseFragment(), View.OnClickListener, QueryUserInfoView, IMineView<RedBagAndCouponBean> {
    override fun loadRedBagAndConpon(data: RedBagAndCouponBean) {
        redBagAndCoupon = data.redPacketsCount + data.rateCount
        tv_coupons_acount.text = "(" + redBagAndCoupon + ")"
    }

    companion object {
        val REQUEST_CODE_SETTING = 300
    }

    private var redBagAndCoupon: Int = 0
    private var mIsShowTotalAssets = false
    private var mIsNeedRefreshUserInfo = true
    private val mPresenter: MinePresenter get() = MinePresenter(this, mStateManager)
    override fun shouldReceiveOtherLogin(): Boolean {
        return false
    }

    override fun getLayoutResourse(): Int {
        return R.layout.fragment_mine
    }

    override fun initView(view: View) {
        tv_recharge.setOnClickListener(this)
        tv_withdraw.setOnClickListener(this)
        iv_home_eyes.setOnClickListener(this)
        rl_financial_details.setOnClickListener(this)
        rl_my_invest.setOnClickListener(this)
        rl_date_of_payment.setOnClickListener(this)
        rl_my_welfare.setOnClickListener(this)
        rl_security_settings.setOnClickListener(this)
        ll_customer_service_center.setOnClickListener(this)
        ll_help_center.setOnClickListener(this)
        rl_feed_back.setOnClickListener(this)
        rl_avatar.setOnClickListener(this)
        iv_question.setOnClickListener(this)
        tv_login_in.setOnClickListener(this)
        ll_invest_friend.setOnClickListener(this)
        tv_coupons_acount.text = ""
    }

    override fun onFirstUserVisible() {
        CommonModule.queryUserInfo(this)
    }

    override fun onUserVisible() {
        if (mIsNeedRefreshUserInfo) {
            CommonModule.queryUserInfo(this)
            mIsNeedRefreshUserInfo = false
        }
    }

    override fun initData() {

    }

    override fun onResume() {
        //super.onResume就会调用OnUserVisible方法，所以状态变更需要放在super.onResume()之前
        mIsNeedRefreshUserInfo = true
        super.onResume()
        if (UUApplication.user == null) {
            ll_mine_contian.visibility = View.VISIBLE
            ll_acount_assest.visibility = View.GONE
            //可用余额未登录是隐藏登入是显示
            rl_mine_total_money.visibility = View.GONE
            tv_coupons_acount.text = ""
        } else {
            ll_acount_assest.visibility = View.VISIBLE
            ll_mine_contian.visibility = View.GONE
            rl_mine_total_money.visibility = View.VISIBLE
            mPresenter.requestRedBagAndCoupon()
        }
        mIsShowTotalAssets = SPUtil.getInstance().getBoolean(Constant.IS_SHOW_TOTAL_ASSETS, true)

        //资产总额
        checkIsShowAssets()
    }

    override fun retryGetData() {
        mStateManager.showLoading()
        CommonModule.queryUserInfo(this)
    }

    override fun queryUserInfoFailure(e: Throwable?, errorCode: Int) {
        NetCommonMethod.judgeError(errorCode, mStateManager)
    }

    override fun queryUserInfoSuccess(data: QueryUserInfoBean) {

    }

    override fun queryBankFailure(e: Throwable?, errorCode: Int) {
        NetCommonMethod.judgeError(errorCode, mStateManager)
    }

    override fun queryBankSuccess(data: QueryBankBean) {

        mStateManager.showContent()
        setValue()
        if (!data.isSuccess) {
            return
        }
        if (data.bankCardEOs == null || data.bankCardEOs.size < 1) {
            return
        }
        if (data.bankCardEOs[0] == null) {
            return
        }
        if (!TextUtils.isEmpty(data.bankCardEOs[0].bankName)) {
            if (UUApplication.user != null) {
                UUApplication.user?.setBankInfo(data.bankCardEOs[0])
            }
        }
    }

    override fun onClick(v: View?) {
        if (LibCommonUtil.isFastDoubleClick()) {
            return
        }
        when (v?.id) {
            R.id.tv_recharge -> {
                //充值
                MobclickAgent.onEvent(context, "wd_cz_btn")
                if (UUApplication.user != null) {
                    goRecharge()
                }
            }
            R.id.tv_withdraw -> {
                //提现
                if (UUApplication.user != null) {
                    MobclickAgent.onEvent(context, "wd_tx_btn")
                    goWithdraw()
                }}
            R.id.ll_invest_friend -> {
                //我的邀请
                MobclickAgent.onEvent(context, "hd_wdyq")
                HtmlUrlActivity.launch(NetConstant.H5_MY_INVITE, true)
            }
            R.id.tv_login_in -> {
                //去登陆
                if (UUApplication.user == null) {
                    LoginActivity.launch()
                }
            }
//            R.id.ll_total_assets -> {-
//                //跳转web，查看个人资产配置
//                MobclickAgent.onEvent(context, "wd_zjmx")
//                HtmlUrlActivity.launch(NetConstant.MINE_MONEY_CHART, true)
//            }
            R.id.rl_avatar -> {
                //去登陆
                if (UUApplication.user == null) {
                    LoginActivity.launch()
                    return
                }
                //个人资料
                ARouter.getInstance().build(ArouterConstant.MINE_PERSONAL_DATA).navigation()
            }
            R.id.iv_question -> {

//                跳转web，查看个人资产配置
                MobclickAgent.onEvent(context, "wd_zjmx")
                HtmlUrlActivity.launch(NetConstant.MINE_MONEY_CHART, true)
            }
            R.id.iv_home_eyes -> {
                //是否显示总资产
                mIsShowTotalAssets = !mIsShowTotalAssets
                SPUtil.getInstance().put(Constant.IS_SHOW_TOTAL_ASSETS, mIsShowTotalAssets)
                checkIsShowAssets()
            }
            R.id.rl_financial_details -> {
                //去登陆
                if (UUApplication.user == null) {
                    LoginActivity.launch()
                    return
                }
                //资金明细
                ARouter.getInstance().build(ArouterConstant.MINE_FINANCIAL_DETAIL).navigation()
            }
            R.id.rl_my_invest -> {
                //去登陆
                if (UUApplication.user == null) {
                    LoginActivity.launch()
                    return
                }
                //我的投资
                MobclickAgent.onEvent(context, "wd_wdtz")
                ARouter.getInstance().build(ArouterConstant.MINE_MY_INVEST).navigation()
            }
            R.id.ll_help_center -> {
                HtmlUrlActivity.launch(NetConstant.H5_HELP_CENTER, false)
            }
            R.id.ll_customer_service_center -> {
                //联系客服
                MobclickAgent.onEvent(context, "hd_jfzx_lxkf")
                DialogFactory.instance.getBottomDialog(R.layout.layout_bottom_dialog_contact_service, object : CommonFragmentDialog.ILogicSetter {
                    override fun setLogic(fragmentDialog: CommonFragmentDialog, rootView: View) {
                        //QQ客服
                        rootView.findViewById<TextView>(R.id.tv_contact_qq_server).setOnClickListener({
                            contackQQServer()
                            fragmentDialog.dismiss()
                        })

                        //电话客服
                        rootView.findViewById<View>(R.id.tv_call_phone).setOnClickListener({
                            fragmentDialog.dismiss()
                            DialogFactory.instance.getPhoneCallDialog(View.OnClickListener {
                                call()
                            }).show(fragmentManager, "contact_server_dialog")
                        })

                        //取消
                        rootView.findViewById<View>(R.id.tv_cancel).setOnClickListener({
                            fragmentDialog.dismiss()
                        })
                    }

                }).show(fragmentManager, "contact_service")
            }
            R.id.rl_date_of_payment -> {
                //去登陆
                if (UUApplication.user == null) {
                    LoginActivity.launch()
                    return
                }
                //回款日程
                MobclickAgent.onEvent(context, "wd_hkjh")
                ARouter.getInstance().build(ArouterConstant.MINE_PAYMENT_SCHEDULE).navigation()
            }
            R.id.rl_my_welfare -> {
                //去登陆
                if (UUApplication.user == null) {
                    LoginActivity.launch()
                    return
                }
                //我的优惠
                MobclickAgent.onEvent(context, "wd_wdjl")
                ARouter.getInstance().build(ArouterConstant.MINE_MY_WELFARE).navigation()
            }
            R.id.rl_security_settings -> {
                //去登陆
                if (UUApplication.user == null) {
                    LoginActivity.launch()
                    return
                }
                //安全设置
                MobclickAgent.onEvent(context, "wd_wd_aqsz")
                ARouter.getInstance().build(ArouterConstant.MINE_SECURITY_SETTING).navigation()
            }
            R.id.rl_feed_back -> {
                //意见反馈
                MobclickAgent.onEvent(context, "hd_feedback")
                ARouter.getInstance().build(ArouterConstant.INFORMATION_FEEDBACK).navigation()
            }
        }
    }

    /*
        在张恒拜祭之后，村长他们也上了香火，拜祭了一番，然后躲在祠堂的一侧商量着什么，大概持续了半小时。
        张二虎在商讨结束后，也由开始的不悦变得笑颜连连。

        ......

        东都有三十六州，他们在南部七州的东池云州九华山地界，


     */
    /**
     * 跳转QQ客服
     */
    private fun contackQQServer() {
        if (LibCommonUtil.isAvilible(context, "com.tencent.mobileqq") || LibCommonUtil.isAvilible(context, "com.tencent.tim")) {
            val urlQQ = "http://wpa.b.qq.com/cgi/wpa.php?ln=2&uin=4006688956"
            HtmlUrlActivity.launch(urlQQ)
        } else {
            ToastUtil.showToast("请先安装腾讯QQ或TIM")
        }
    }

    /**
     * 联系客服，打电话
     */
    private fun call() {
        AndPermission.with(this)
                .requestCode(200)
                .permission(Manifest.permission.CALL_PHONE)
                .callback(permissionListener)
                // rationale作用是：用户拒绝一次权限，再次申请时先征求用户同意，再打开授权对话框；
                // 这样避免用户勾选不再提示，导致以后无法申请权限。
                // 你也可以不设置。
                .rationale({ requestCode, rationale ->
                    // 这里的对话框可以自定义，只要调用rationale.resume()就可以继续申请。
                    AndPermission.rationaleDialog(context, rationale).show()
                })
                .start()
    }

    private val permissionListener = object : PermissionListener {
        override fun onSucceed(requestCode: Int, grantPermissions: List<String>) {

            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getString(R.string.CALL_PHONE)))
            try {
                startActivity(intent)
            } catch (e: SecurityException) {
                LogUtil.e(e.toString())
            }
        }

        override fun onFailed(requestCode: Int, deniedPermissions: List<String>) {

            // 用户否勾选了不再提示并且拒绝了权限，那么提示用户到设置中授权。
            if (AndPermission.hasAlwaysDeniedPermission(context, deniedPermissions)) {
                // 第一种：用默认的提示语。
                AndPermission.defaultSettingDialog(activity, REQUEST_CODE_SETTING).show()
            }
        }
    }

    /**
     * 跳转充值界面
     */
    private fun goRecharge() {
        //跳转实名认证
        if (!UUApplication.user?.isIsIdentityUserInfo()!!) {
            ARouter.getInstance().build(ArouterConstant.UUBASE_IDENTIFICATION).navigation()
            return
        }
        ARouter.getInstance().build(ArouterConstant.MINE_RECHARGE).navigation()
    }

    /**
     * 跳转提现界面
     */
    private fun goWithdraw() {
        if (UUApplication.user == null) {
            ToastUtil.showToast(R.string.please_login_first)
            return
        }

        //跳转实名认证页面
        if (!UUApplication.user?.isIsIdentityUserInfo()!!) {
            ARouter.getInstance().build(ArouterConstant.UUBASE_IDENTIFICATION).navigation()
            return
        }

        //跳转绑卡页面
        if (!UUApplication.user?.isIsBandCard()!!) {
            ARouter.getInstance().build(ArouterConstant.MINE_BIND_BANK_CARD).navigation()
            return
        }

        //跳转支付密码设置页面
        if (!UUApplication.user?.isIsTradersPwBinded()!!) {
            ARouter.getInstance().build(ArouterConstant.MINE_SET_TRADING_PWD).navigation()
            return
        }
        ARouter.getInstance().build(ArouterConstant.MINE_WITHDRAW).navigation()
    }

    /**
     * 设置是否显示资产总额
     */
    private fun checkIsShowAssets() {
        LogUtil.e("yexm+mIsShowTotalAssets", mIsShowTotalAssets.toString())
        if (mIsShowTotalAssets) {
            iv_home_eyes.setImageResource(R.mipmap.icon_eyes_open)
            if (UUApplication.user != null) {
                tv_total_assets.text = NumberFormatUtils.formatNumberWithComma(NumberFormatUtils.getNumberWithDigital(UUApplication.user?.getTotalMoney()!!, 2))
                tv_accumulated_earning_number.text = NumberFormatUtils.formatNumberWithComma(NumberFormatUtils.getNumberWithDigital(UUApplication.user?.getHasIncome()!!, 2))
                tv_online_investment.text = NumberFormatUtils.formatNumberWithComma(NumberFormatUtils.getNumberWithDigital(UUApplication.user?.getInvestingAmount()!! + UUApplication.user?.getFreezingAmount()!!, 2))
                tv_value_available_balance.text = NumberFormatUtils.formatNumberWithComma(NumberFormatUtils.getNumberWithDigital(UUApplication.user?.getBalanceMoney()!!, 2))
            }
        } else {
            iv_home_eyes.setImageResource(R.mipmap.icon_eyes_close)
            tv_total_assets.text = "*****"
            tv_online_investment.text = "*****"
            tv_accumulated_earning_number.text = "*****"
            tv_value_available_balance.text = "*****"
        }
    }

    /**
     * 设置网络请求回来的数据
     */
    private fun setValue() {

        if (UUApplication.user == null) {
            return
        }
        if (mIsShowTotalAssets) {
            //累计收益
            tv_accumulated_earning_number.text = NumberFormatUtils.formatNumberWithComma(NumberFormatUtils.getNumberWithDigital(UUApplication.user?.getHasIncome()!!, 2))
            tv_total_assets.text = NumberFormatUtils.formatNumberWithComma(NumberFormatUtils.getNumberWithDigital(UUApplication.user?.getTotalMoney()!!, 2))
            tv_online_investment.text = NumberFormatUtils.formatNumberWithComma(NumberFormatUtils.getNumberWithDigital(UUApplication.user?.getInvestingAmount()!! + UUApplication.user?.getFreezingAmount()!!, 2))
            //可用余额
            tv_value_available_balance.text = NumberFormatUtils.formatNumberWithComma(NumberFormatUtils.getNumberWithDigital(UUApplication.user?.getBalanceMoney()!!, 2))
        }
    }

}
